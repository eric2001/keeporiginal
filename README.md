# KeepOriginal

## Description
KeepOriginal is a module for Gallery 3 which will allow Gallery to create a backup copy of the original photo before modifying it.  Currently, Gallery 3 only modifies photos when rotating them, but should that change KeepOriginal could be expanded to cover new scenarios as well.

**License:** [GPL v.2](http://www.gnu.org/licenses/old-licenses/gpl-2.0.html)

## Instructions
To install, extract the "keeporiginal" folder from the zip file into your Gallery 3 modules folder. Afterwards log into your Gallery web site as an administrator and activate the module in the Admin -> Modules menu.  Any photo modified after KeepOriginal is installed will be given an Options->Restore original menu option to revert back to the previous version.

## History
**Version 1.2.0:**
> - Updated for recent module API changes in Gallery 3.
> - Released 20 January 2010.
>
> Download: [Version 1.2.0](/uploads/018d5bc4d31cdd83614a441900316a15/keeporiginal120.zip)

**Version 1.1.2:**
> - Bug Fix:  When moving a photo make sure the destination path in VARPATH/original exists, create it if it doesn't.
> - Released 30 September 2009 
>
> Download: [Version 1.1.2](/uploads/6e4eb09316771dc474eabb2ac6069877/keeporiginal112.zip)

**Version 1.1.1:**
> - Updated to allow for language translations.
> - Released on 08 August 2009
>
> Download: [Version 1.1.1](/uploads/6a427abed8a499398f109f431f112f53/keeporiginal111.zip)

**Version 1.1.0:**
> - Updated for changes in git.
> - Added additional error handling code.
> - Released on 03 August 2009
>
> Download: [Version 1.1.0](/uploads/4adec19020ab983640ef2008837b22df/keeporiginal110.zip)

**Version 1.0.0:**
> - Initial Release
> - Released on 29 July 2009
>
> Download: [Version 1.0.0](/uploads/61f7df4a03ee8dfb9b82babad5b3e58a/keeporiginal100.zip)
